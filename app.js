const express = require('express');
const bodyParser = require('body-parser');
const dotenv = require('dotenv');
dotenv.config();

const exec = require('./config/database');

const app = express();
const port = process.env.PORT || 3000;

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header(
    'Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept'
  );
  next();
});

app.listen(port, () => {
  console.log(`Server ON: http://localhost:${port}`);
});

// Routes
app.get('/', async (req, res) => {
  try {
    const query = 'SELECT * FROM personas';
    const data = await exec(query);
    res.status(200).json({ data: data });
  } catch (e) {
    console.error(e.message);
    res.status(413).json({ message: 'Error server!' });
  }
});

app.post('/persona', (req, res) => {
  console.log(req.body.nombre);
  res.status(200).json(req.body);
});

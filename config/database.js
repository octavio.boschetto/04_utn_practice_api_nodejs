const mysql = require('mysql');
const util = require('util');

const connection = mysql.createConnection({
  host: process.env.DB_HOST,
  user: process.env.DB_USERNAME,
  password: process.env.DB_PASSWORD,
  database: process.env.DB_DATABASE,
});

connection.connect((err) => {
  if (err) {
    throw err;
  }
  console.log('Se ha establecido la conexion a la DB.');
});

const exec = util.promisify(connection.query).bind(connection);

module.exports = exec;
